mod sort {
    mod insertion_sort {

        pub fn sort<T: Ord>(xs: &mut Vec<T>) {
            for i in 1..xs.len() {
                let mut j = i;
                while (j > 0) && (xs[j] < xs[j - 1]) {
                    xs.swap(j, j - 1);
                    j = j - 1;
                }
            }
        }

        #[cfg(test)]
        mod tests {
            use super::*;

            #[test]
            fn sort_empty_vec() {
                let mut xs : Vec<i32> = vec!();
                sort(&mut xs);
                assert_eq!(vec!() as Vec<i32>, xs);
            }

            #[test]
            fn sort_single_element_vec() {
                let mut xs = vec!(1);
                sort(&mut xs);
                assert_eq!(vec!(1), xs);
            }

            #[test]
            fn sort_reversed_vec() {
                let mut xs = vec!(5, 4, 3, 2, 1);
                sort(&mut xs);
                assert_eq!(vec!(1, 2, 3, 4, 5), xs);
            }
        }

    }

}