mod sort {
    mod selection_sort{

        /// Sorts a vector of integers.
        ///
        pub fn sort<T: Ord>(xs: &mut Vec<T>) {
            // index of minimum
            let mut min;
            for i in 0..xs.len() {
                min = i;
                for j in (i + 1)..xs.len() {
                    if xs[j] < xs[min] {
                        min = j;
                    }
                }
                xs.swap(i, min);
            }
        }

        #[cfg(test)]
        mod tests {
            use super::*;

            #[test]
            fn sort_empty_vec() {
                let mut xs : Vec<i32> = vec!();
                sort(&mut xs);
                assert_eq!(vec!() as Vec<i32>, xs);
            }

            #[test]
            fn sort_single_element_vec() {
                let mut xs = vec!(1);
                sort(&mut xs);
                assert_eq!(vec!(1), xs);
            }

            #[test]
            fn sort_reversed_vec() {
                let mut xs = vec!(5, 4, 3, 2, 1);
                sort(&mut xs);
                assert_eq!(vec!(1, 2, 3, 4, 5), xs);
            }
        }

    }

}