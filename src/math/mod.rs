mod math {

    pub fn power(a: i32, n: i32) -> i32 {
        if n == 0 {
            return 1;
        }
        let x = power(a, n / 2);
        if n % 2 == 0 {
            return x * x;
        } else {
            return a * x * x;
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_power() {
            assert_eq!(1_i32.pow(1), power(1, 1));
            assert_eq!(2_i32.pow(2), power(2, 2));
            assert_eq!(3_i32.pow(3), power(3, 3));
            assert_eq!(4_i32.pow(4), power(4, 4));
        }

    }
}