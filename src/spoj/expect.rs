// https://www.spoj.com/problems/EXPECT/

use std::io::{self, Read};

pub fn main() {
    let mut finished = false;
    while !finished {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        print!("{}", buffer);
        finished = buffer.trim() == "42"
    }
}
