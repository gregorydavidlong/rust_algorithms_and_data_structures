// https://www.spoj.com/problems/TEST/

use std::io::{self, Read};

pub fn main() {
    let mut finished = false;
    while !finished {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        finished = buffer.trim() == "42";
        if !finished {
            print!("{}", buffer);
        }
    }
}
