// https://www.spoj.com/problems/PRIME1/
use std::io::{self};

// DOESN'T WORK????

// https://en.wikipedia.org/wiki/Primality_test
fn is_prime(n: i32) -> bool {
    if n <= 3 {
        return n > 1
    } else if (n % 2 == 0) || (n % 3 == 0) {
        return false
    }
    let mut i = 5;
    while i * i < n {
        if (n % i == 0) || (n % (i + 2) == 0) {
            return false
        }
        i = i + 6
    }
    return true
}

pub fn main() {
    let mut num_tests_str= String::new();
    io::stdin().read_line(&mut num_tests_str).unwrap();
    let num_tests = num_tests_str.trim().parse::<i32>().expect("Invalid num tests!");
    for test_num in 0..num_tests {
        let mut num_bounds_str= String::new();
        io::stdin().read_line(&mut num_bounds_str).unwrap();
        let mut i = num_bounds_str.split_whitespace();
        let lower_bound = i.next().unwrap().trim().parse::<i32>().expect("Invalid lower bound");
        let upper_bound = i.next().unwrap().trim().parse::<i32>().expect("Invalid upper bound");
        for num in lower_bound..(upper_bound + 1) {
            if is_prime(num) {
                println!("{}", num)
            }
        }
        if test_num != num_tests - 1 {
            println!()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_prime() {
        assert_eq!(true, is_prime(7));
        assert_eq!(false, is_prime(6));
    }

}