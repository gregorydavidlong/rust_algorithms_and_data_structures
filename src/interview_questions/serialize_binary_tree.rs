mod interview_questions {
    mod serialize_binary_tree {

        #[derive(Debug, PartialEq)]
        pub struct Node {
            val: i32,
            l: Option<Box<Node>>,
            r: Option<Box<Node>>,
        }

        pub fn serialize(node: &Node) -> String {
            format!("( {} {} {} )",
                    node.val,
                    serialize_alt(&node.l),
                    serialize_alt(&node.r))
        }

        fn serialize_alt(node: &Option<Box<Node>>) -> String {
            match node {
                Some(ref n) => serialize(n),
                None => ".".to_string()
            }
        }

        pub fn deserialize(input: String) -> Result<Node, String> {
            let mut i = input.split_whitespace();
            match deserialize_alt(&mut i) {
                Ok(Some(n)) => Ok(*n),
                Ok(None) => Err(format!("No nodes found in input: {}", input)),
                Err(s) => Err(s)
            }
        }

        fn deserialize_alt<'a, I>(i: &mut I) -> Result<Option<Box<Node>>, String>
            where I: Iterator<Item = &'a str> {
            match i.next() {
                Some(".") => Ok(None),
                Some("(") => {
                    let opt_val = i.next().ok_or("Ran out of input".to_string())?;
                    let val = opt_val.parse::<i32>()
                        .or(Err(format!("Cannot parse: {}", opt_val)))?;
                    let opt_l = deserialize_alt(i)?;
                    let opt_r = deserialize_alt(i)?;
                    i.next(); // )
                    Ok(Some(Box::new(Node {
                        val,
                        l: opt_l,
                        r: opt_r
                    })))
                },
                Some(s) => Err(format!("Invalid input at: {}", s)),
                None => Ok(None)
            }
        }

        #[cfg(test)]
        mod tests {
            use super::*;

            #[test]
            fn test_node() {
                let node = Node { val: 1, l: None, r: None };
                let serialized = serialize(&node);
                assert_eq!("( 1 . . )", serialize(&node));
                assert_eq!(Ok(node), deserialize(serialized))
            }

            #[test]
            fn test_node2() {
                let node = Node {
                    val: 1,
                    l: Some(Box::new(Node {
                        val: 2,
                        l: None,
                        r: Some(Box::new(Node {
                            val: 4,
                            l: None,
                            r: None
                        }))
                    })),
                    r: Some(Box::new(Node {
                        val: 3,
                        l : Some(Box::new(Node {
                            val: 42,
                            l: None,
                            r: None
                        })),
                        r : Some(Box::new(Node {
                            val: 15,
                            l: None,
                            r: None
                        }))
                    }))};
                let serialized = serialize(&node);
                assert_eq!("( 1 ( 2 . ( 4 . . ) ) ( 3 ( 42 . . ) ( 15 . . ) ) )", serialized);
                assert_eq!(Ok(node), deserialize(serialized))
            }

            #[test]
            fn test_deserialize_when_no_val() {
                assert_eq!(Err("Ran out of input".to_string()), deserialize("( ".to_string()))
            }

            #[test]
            fn test_deserialize_when_invalid_val() {
                assert_eq!(Err("Cannot parse: a".to_string()), deserialize("( a . . )".to_string()))
            }

            #[test]
            fn test_deserialize_when_invalid_structure() {
                assert_eq!(Err("Invalid input at: 2".to_string()), deserialize("( 1 2 3 )".to_string()))
            }

            #[test]
            fn test_invalid_deserialize_when_empty_input() {
                assert_eq!(Err("No nodes found in input: ".to_string()), deserialize("".to_string()))
            }
        }
    }
}