
// Given a string "12345", general all possible IP addresses, e.g.:
// 1.2.3.45
// 1.2.34.5
// 1.23.4.5
// 12.3.4.5

pub fn generate_ips(input: &String) -> Vec<String> {
    let mut acc = vec![];
    generate_ips_alt(input, vec![], &mut acc);
    acc
}

fn generate_ips_alt(input: &String, current: Vec<String>, acc: &mut Vec<String>) {
    if current.len() != 4 {
        if input.len() > 0 {
            let segment: String = input.chars().take(1).collect();
            let new_input: String = input.chars().skip(1).collect();
            let mut new_current = current.clone();
            new_current.push(segment);
            generate_ips_alt(&new_input, new_current, acc)
        }
        if input.len() > 1 {
            let segment: String = input.chars().take(2).collect();
            let new_input: String = input.chars().skip(2).collect();
            let mut new_current = current.clone();
            new_current.push(segment);
            generate_ips_alt(&new_input, new_current, acc)
        }
        if input.len() > 2 {
            let segment: String = input.chars().take(3).collect();
            if is_valid_ip_segment(&segment) {
                let new_input: String = input.chars().skip(3).collect();
                let mut new_current = current.clone();
                new_current.push(segment);
                generate_ips_alt(&new_input, new_current, acc)
            }
        }
    } else {
        acc.push(current.join("."))
    }
}

fn is_valid_ip_segment(seg: &String) -> bool {
    let seg_int = seg.parse::<i32>().unwrap();
    seg_int < 256
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_ips() {
        assert_eq!(vec!["1.2.3.4".to_string(),
                        "1.2.3.45".to_string(),
                        "1.2.34.5".to_string(),
                        "1.23.4.5".to_string(),
                        "12.3.4.5".to_string()],
                   generate_ips(&"12345".to_string()));
        assert_eq!(vec!["5.5.5.2".to_string(),
                        "5.5.5.23".to_string(),
                        "5.5.5.234".to_string(),
                        "5.5.52.3".to_string(),
                        "5.5.52.34".to_string(),
                        "5.55.2.3".to_string(),
                        "5.55.2.34".to_string(),
                        "5.55.23.4".to_string(),
                        "55.5.2.3".to_string(),
                        "55.5.2.34".to_string(),
                        "55.5.23.4".to_string(),
                        "55.52.3.4".to_string()],
                   generate_ips(&"555234".to_string()))
    }
}
