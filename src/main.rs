extern crate rust_algorithms_and_data_structures;

use rust_algorithms_and_data_structures::spoj;

fn main() {
    spoj::prime1::main()
}
